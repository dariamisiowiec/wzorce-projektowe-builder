package zadanie2;

import java.time.LocalDate;

public class Mail {
    private String tresc;
    private String nadawca;
    private LocalDate dataNadania;
    private LocalDate dataOdbioru;
    private String adresIPnadania;
    private String adresIPodebrania;
    private String nazwaSerweraPosredniego;
    private String nazwaSkrzynkiPocztowej;
    private String protokolKomunikacji;
    private TypWiadomosci typWiadomosci;
    private boolean czySzyfrowane;
    private boolean czySpam;


}
