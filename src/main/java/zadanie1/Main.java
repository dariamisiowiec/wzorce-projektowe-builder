package zadanie1;

import java.util.LinkedList;

public class Main {
    public static void main(String[] args) {
        LinkedList<GameCharacter> myHeroes = new LinkedList<GameCharacter>();
        GameCharacter.Builder builder = new GameCharacter.Builder();
        GameCharacter character = builder.setHealth(1000).createGameCharacter();
        GameCharacter character2 = builder.createGameCharacter();
        GameCharacter character3 = builder.setName("XD").createGameCharacter();
        GameCharacter character4 = builder.setMana(20).setName("QWERTY").createGameCharacter();
        myHeroes.add(character);
        myHeroes.add(character2);
        myHeroes.add(character3);
        myHeroes.add(character4);

        myHeroes.add(builder.setName("Z").setHealth(-9292).setNumberOfPoints(3).createGameCharacter());

        for (int i = 0; i < myHeroes.size(); i++) {
            System.out.println(myHeroes.get(i));
        }

    }
}
